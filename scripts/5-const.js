function allyIlliterate() {

  for( let tuce = 0; tuce < 5; tuce++ ) {
      //tuce is only visible in here (and in the for() parentheses)
      //and there is a separate tuce variable for each iteration of the loop
  }

  //tuce is *not* visible out here
}

function byE40() {

  for( var nish = 0; nish < 5; nish++ ) {
      //nish is visible to the whole function
  }

  //nish *is* visible out here
}

// Scalar values
const answer = 42;
const greeting = 'Hello';

// Arrays and Objects
const numbers = [2, 4, 6];
const person = {
  firstName: 'John',
  lastName: 'Doe',
};
